<!DOCTYPE html>
<html>
<head>
	<title>oop</title>
</head>
<body>
	<?php
	require ('animal.php');
	require ('ape.php');
	require ('frog.php');

	$sheep = new Animal("shaun");

	echo $sheep->name; // "shaun"
	echo "<br>";
	echo $sheep->legs; // 2
	echo "<br>";
	echo $sheep->cold_blooded; // false
	echo "<br><br>";

	$sungokong = new Ape("kera sakti");
	$sungokong->yell(); // "Auooo"

	echo "<br><br>";

	$kodok = new Frog("buduk");
	$kodok->jump() ; // "hop hop"
	?>

</body>
</html>